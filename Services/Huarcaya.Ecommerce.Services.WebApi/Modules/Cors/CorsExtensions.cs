using System.IdentityModel.Tokens.Jwt;

namespace Huarcaya.Ecommerce.Services.WebApi.Modules.Cors;

public static class CorsExtensions
{
    public static IServiceCollection AddCors(this IServiceCollection services, IConfiguration configuration, string myPolicy)
    {
        JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
        JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

        services.AddCors(
            options => options.AddPolicy(
                myPolicy, corsPolicyBuilder => corsPolicyBuilder
                    .WithOrigins(configuration.GetValue<string>("Config:OriginCors"))
                    .AllowAnyHeader()
                    .AllowAnyMethod()
            )
        );

        return services;
    }
}