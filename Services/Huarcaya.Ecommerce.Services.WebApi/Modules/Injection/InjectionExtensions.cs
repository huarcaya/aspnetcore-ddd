using Huarcaya.Ecommerce.Application.Interface;
using Huarcaya.Ecommerce.Application.Main;
using Huarcaya.Ecommerce.Domain.Core;
using Huarcaya.Ecommerce.Domain.Interface;
using Huarcaya.Ecommerce.Infrastructure.Data;
using Huarcaya.Ecommerce.Infrastructure.Interface;
using Huarcaya.Ecommerce.Infrastructure.Repository;
using Huarcaya.Ecommerce.Transversal.Common;
using Huarcaya.Ecommerce.Transversal.Logging;

namespace Huarcaya.Ecommerce.Services.WebApi.Modules.Injection;

public static class InjectionExtensions
{
    public static IServiceCollection AddInjection(this IServiceCollection services)
    {
        services.AddSingleton<IConnectionFactory, ConnectionFactory>();
        services.AddScoped<ICustomerApplication, CustomerApplication>();
        services.AddScoped<ICustomerDomain, CustomerDomain>();
        services.AddScoped<ICustomerRepository, CustomerRepository>();
        services.AddScoped<IUserApplication, UserApplication>();
        services.AddScoped<IUserDomain, UserDomain>();
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped(typeof(IAppLogger<>), typeof(LoggerAdapter<>));

        return services;
    }
}