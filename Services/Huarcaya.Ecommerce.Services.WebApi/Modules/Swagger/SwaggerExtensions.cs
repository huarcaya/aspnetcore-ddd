using Microsoft.OpenApi.Models;

namespace Huarcaya.Ecommerce.Services.WebApi.Modules.Swagger;

public static class SwaggerExtensions
{
    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v1",
                Title = "Huarcaya API Market",
                Description = "A simple example of ASP.NET Core Web API",
                TermsOfService = new System.Uri("https://huarcaya.com/license/"),
                Contact = new OpenApiContact
                {
                    Name = "Gustavo Adolfo Huarcaya Delgado",
                    Email = "diavolo@gahd.net",
                    Url = new System.Uri("https://gahd.net")
                },
                License = new OpenApiLicense
                {
                    Name = "Use under License",
                    Url = new System.Uri("http://huarcaya.com/license/")
                }
            });
            // https://learn.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-6.0&tabs=visual-studio
            options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Name = "Authorization",
                Type = SecuritySchemeType.ApiKey,
                Scheme = "Bearer",
                BearerFormat = "JWT",
                In = ParameterLocation.Header
            });
            options.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                new string[]{}
                }
            });
        });
        return services;
    }
}