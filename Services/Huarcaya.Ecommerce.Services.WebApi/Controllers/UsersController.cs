using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

using BC = BCrypt.Net.BCrypt;

using Huarcaya.Ecommerce.Application.DTO;
using Huarcaya.Ecommerce.Application.Interface;
using Huarcaya.Ecommerce.Services.WebApi.Helpers;
using Huarcaya.Ecommerce.Transversal.Common;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Huarcaya.Ecommerce.Services.WebApi.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]")]
public class UsersController : ControllerBase
{
    private readonly IUserApplication _userApplication;
    private readonly IConfiguration _configuration;
    // private readonly IOptions<AppSettings> _appSettings;

    public UsersController(IUserApplication userApplication, IConfiguration configuration /* IOptions<AppSettings> appSettings */)
    {
        _userApplication = userApplication;
        _configuration = configuration;
        // _appSettings = appSettings;
    }

    [AllowAnonymous]
    [HttpPost("login")]
    public IActionResult Authenticate([FromBody] LoginUserDto loginUserDto)
    {
        var response = _userApplication.Authenticate(loginUserDto.Username, loginUserDto.Password);

        if (response.IsSuccess)
        {
            if (response.Data == null)
            {
                return Unauthorized(response.Message);
            }

            response.Data.Token = BuildToken(response);

            return Ok(response);
        }

        return Unauthorized(response.Message);
    }

    [AllowAnonymous]
    [HttpPost]
    public IActionResult CreateNewUser([FromBody] NewUserDto newUserDto)
    {
        newUserDto.Password = BC.HashPassword(newUserDto.Password);
        var response = _userApplication.CreateNewUSer(newUserDto.Username,
            newUserDto.Password, newUserDto.FirstName, newUserDto.LastName);

        if (response.IsSuccess)
        {
            if (response.Data == null)
            {
                return BadRequest(response.Message);
            }

            return Ok(response);
        }

        return BadRequest(response.Message);
    }

    private string BuildToken(Response<UserDto> userDto)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        // var key = Encoding.ASCII.GetBytes(_appSettings.Value.Secret);
        var key = Encoding.ASCII.GetBytes(_configuration["Config:Secret"] ?? "qwerty");
        // var key = Encoding.ASCII.GetBytes("querty");
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new Claim[]{
                new Claim(ClaimTypes.Name, userDto.Data.UserId.ToString())
            }),
            Expires = DateTime.UtcNow.AddMinutes(1),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            // Issuer = _appSettings.Value.Issuer,
            // Audience = _appSettings.Value.Audience,
            Issuer = _configuration["Config:Issuer"],
            Audience = _configuration["Config:Audience"],
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var tokenString = tokenHandler.WriteToken(token);

        return tokenString;
    }
}