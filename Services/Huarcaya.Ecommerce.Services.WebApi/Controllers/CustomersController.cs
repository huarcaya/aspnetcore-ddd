using Huarcaya.Ecommerce.Application.DTO;
using Huarcaya.Ecommerce.Application.Interface;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Huarcaya.Ecommerce.Services.WebApi.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]")]
public class CustomersController : ControllerBase
{
    private readonly ICustomerApplication _customerApplication;

    public CustomersController(ICustomerApplication customerApplication)
    {
        _customerApplication = customerApplication;
    }

    #region Sync Methods

    [HttpPost]
    public IActionResult Insert([FromBody] CustomerDto customerDTO)
    {
        if (customerDTO == null)
        {
            return BadRequest();
        }

        var response = _customerApplication.Insert(customerDTO);

        if (response.IsSuccess)
        {
            return Ok(response);
        }

        return BadRequest(response.Message);
    }

    [HttpPut]
    public IActionResult Update([FromBody] CustomerDto customerDTO)
    {
        if (customerDTO == null)
        {
            return BadRequest();
        }

        var response = _customerApplication.Update(customerDTO);

        if (response.IsSuccess)
        {
            return Ok(response);
        }

        return BadRequest(response.Message);
    }

    [HttpDelete("{customerId}")]
    public IActionResult Delete(string customerId)
    {
        if (string.IsNullOrEmpty(customerId))
        {
            return BadRequest();
        }

        var response = _customerApplication.Delete(customerId);

        if (response.IsSuccess)
        {
            return Ok(response);
        }

        return BadRequest(response.Message);
    }

    [HttpGet("{customerId}")]
    public IActionResult Get(string customerId)
    {
        if (string.IsNullOrEmpty(customerId))
        {
            return BadRequest();
        }

        var response = _customerApplication.Get(customerId);

        if (response.IsSuccess)
        {
            return Ok(response);
        }

        return BadRequest(response.Message);
    }

    [AllowAnonymous]
    [HttpGet]
    public IActionResult GetAll()
    {
        var response = _customerApplication.GetAll();

        if (response.IsSuccess)
        {
            return Ok(response);
        }

        return BadRequest(response.Message);
    }

    #endregion Sync Methods

    #region Async Methods

    [HttpPost("insert")]
    public async Task<IActionResult> InsertAsync([FromBody] CustomerDto customerDTO)
    {
        if (customerDTO == null)
        {
            return BadRequest();
        }

        var response = await _customerApplication.InsertAsync(customerDTO);

        if (response.IsSuccess)
        {
            return Ok(response);
        }

        return BadRequest(response.Message);
    }

    [HttpPut("update")]
    public async Task<IActionResult> UpdateAsync([FromBody] CustomerDto customerDTO)
    {
        if (customerDTO == null)
        {
            return BadRequest();
        }

        var response = await _customerApplication.UpdateAsync(customerDTO);

        if (response.IsSuccess)
        {
            return Ok(response);
        }

        return BadRequest(response.Message);
    }

    [HttpDelete("delete/{customerId}")]
    public async Task<IActionResult> DeleteAsync(string customerId)
    {
        if (string.IsNullOrEmpty(customerId))
        {
            return BadRequest();
        }

        var response = await _customerApplication.DeleteAsync(customerId);

        if (response.IsSuccess)
        {
            return Ok(response);
        }

        return BadRequest(response.Message);
    }

    [HttpGet("get/{customerId}")]
    public async Task<IActionResult> GetAsync(string customerId)
    {
        if (string.IsNullOrEmpty(customerId))
        {
            return BadRequest();
        }

        var response = await _customerApplication.GetAsync(customerId);

        if (response.IsSuccess)
        {
            return Ok(response);
        }

        return BadRequest(response.Message);
    }

    [HttpGet("get-all")]
    public async Task<IActionResult> GetAllAsync()
    {
        var response = await _customerApplication.GetAllAsync();

        if (response.IsSuccess)
        {
            return Ok(response);
        }

        return BadRequest(response.Message);
    }

    #endregion Async Methods
}