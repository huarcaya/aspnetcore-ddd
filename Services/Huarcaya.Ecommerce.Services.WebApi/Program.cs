using Huarcaya.Ecommerce.Services.WebApi.Modules.Authentication;
using Huarcaya.Ecommerce.Services.WebApi.Modules.Cors;
using Huarcaya.Ecommerce.Services.WebApi.Modules.Injection;
using Huarcaya.Ecommerce.Transversal.Mapper;

var builder = WebApplication.CreateBuilder(args);

string myPolicy = "policyApiEcommerce";

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddAuthentication(builder.Configuration);

builder.Services.AddSwaggerGen();
builder.Services.AddCors(builder.Configuration, myPolicy);

builder.Services.AddRouting(options => options.LowercaseUrls = true);

/* var appSettingsSection = builder.Configuration.GetValue<IConfiguration>("Config");
builder.Services.Configure<AppSettings>(appSettingsSection); */

builder.Services.AddAutoMapper(x => x.AddProfile(new MappingProfile()));
builder.Services.AddInjection();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// app.UseHttpsRedirection();

app.UseCors(myPolicy);
// app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
