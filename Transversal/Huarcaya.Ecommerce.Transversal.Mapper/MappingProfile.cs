﻿using AutoMapper;

using Huarcaya.Ecommerce.Application.DTO;
using Huarcaya.Ecommerce.Domain.Entity;

namespace Huarcaya.Ecommerce.Transversal.Mapper;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<Customer, CustomerDto>().ReverseMap();

        /* CreateMap<Customer, CustomerDto>().ReverseMap()
        .ForMember(destination => destination.CustomerId, source => source.MapFrom(src => src.CustomerId))
        .ForMember(destination => destination.CompanyName, source => source.MapFrom(src => src.CompanyName))
        .ForMember(destination => destination.ContactName, source => source.MapFrom(src => src.ContactName))
        .ForMember(destination => destination.ContactTitle, source => source.MapFrom(src => src.ContactTitle))
        .ForMember(destination => destination.Address, source => source.MapFrom(src => src.Address))
        .ForMember(destination => destination.City, source => source.MapFrom(src => src.City))
        .ForMember(destination => destination.Region, source => source.MapFrom(src => src.Region))
        .ForMember(destination => destination.PostalCode, source => source.MapFrom(src => src.PostalCode))
        .ForMember(destination => destination.Country, source => source.MapFrom(src => src.Country))
        .ForMember(destination => destination.Phone, source => source.MapFrom(src => src.Phone))
        .ForMember(destination => destination.Fax, source => source.MapFrom(src => src.Fax)); */

        CreateMap<User, UserDto>()
        .ForMember(destination => destination.UserId, source => source.MapFrom(src => src.user_id))
        .ForMember(destination => destination.Username, source => source.MapFrom(src => src.username))
        .ForMember(destination => destination.Password, source => source.MapFrom(src => src.password))
        .ForMember(destination => destination.FirstName, source => source.MapFrom(src => src.first_name))
        .ForMember(destination => destination.LastName, source => source.MapFrom(src => src.last_name))
        .ForMember(destination => destination.Token, source => source.MapFrom(src => src.token))
        .ReverseMap();
    }
}
