using System.Data;

namespace Huarcaya.Ecommerce.Transversal.Common;

public interface IConnectionFactory
{
    IDbConnection GetDbConnection { get; }
}