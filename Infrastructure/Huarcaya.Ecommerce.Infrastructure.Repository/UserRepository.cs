using Dapper;

using Huarcaya.Ecommerce.Domain.Entity;
using Huarcaya.Ecommerce.Infrastructure.Interface;
using Huarcaya.Ecommerce.Transversal.Common;

namespace Huarcaya.Ecommerce.Infrastructure.Repository;

public class UserRepository : IUserRepository
{
    private readonly IConnectionFactory _connectionFactory;

    public UserRepository(IConnectionFactory connectionFactory)
    {
        _connectionFactory = connectionFactory;
    }

    public User FindOneByUsername(string username)
    {
        using var connection = _connectionFactory.GetDbConnection;

        var query = @"SELECT user_id, first_name, last_name, username, password
                        FROM users
                        WHERE username = @username";
        var user = connection.QueryFirstOrDefault<User>(query, new
        {
            username = new DbString { Value = username }
        });

        return user;
    }

    public User CreateNewUSer(string username, string password, string first_name, string last_name)
    {
        using var connection = _connectionFactory.GetDbConnection;

        var query = @"INSERT INTO users (username, password, first_name, last_name)
                        VALUES (@username, @password, @first_name, @last_name);";
        var affectedRows = connection.Execute(query, new
        {
            username = new DbString { Value = username },
            password = new DbString { Value = password },
            first_name = new DbString { Value = first_name },
            last_name = new DbString { Value = last_name },
        });

        var sql = @"SELECT user_id, username, password, first_name, last_name
                    FROM users
                    WHERE username=@username";
        var user = connection.QueryFirst<User>(sql, new { username = new DbString { Value = username } });

        return user;
    }
}