﻿using Dapper;

using Huarcaya.Ecommerce.Domain.Entity;
using Huarcaya.Ecommerce.Infrastructure.Interface;
using Huarcaya.Ecommerce.Transversal.Common;

using System.Data;

namespace Huarcaya.Ecommerce.Infrastructure.Repository;

public class CustomerRepository : ICustomerRepository
{
    private readonly IConnectionFactory _connectionFactory;

    public CustomerRepository(IConnectionFactory connectionFactory)
    {
        this._connectionFactory = connectionFactory;
    }

    #region Sync Methods

    public bool Insert(Customer customer)
    {
        using var connection = _connectionFactory.GetDbConnection;
        // var query = "customers_insert";

        /*var parameters = new DynamicParameters();
        parameters.Add("customer_id", customer.customer_id);
        parameters.Add("company_name", customer.company_name);
        parameters.Add("contact_name", customer.contact_name);
        parameters.Add("contact_title", customer.contact_title);
        parameters.Add("address", customer.address);
        parameters.Add("city", customer.city);
        parameters.Add("region", customer.region);
        parameters.Add("postal_code", customer.postal_code);
        parameters.Add("country", customer.country);
        parameters.Add("phone", customer.phone);
        parameters.Add("fax", customer.fax);*/

        // var affectedRows = connection.Execute(query, param: parameters, commandType: CommandType.StoredProcedure);

        var query = @"INSERT INTO customers (customer_id, company_name, contact_name, contact_title, address, city, region, postal_code, country, phone, fax)
                        VALUES(@customer_id, @company_name, @contact_name, @contact_title, @address, @city, @region, @postal_code, @country, @phone, @fax);";
        var affectedRows = connection.Execute(query, new
        {
            customer_id = customer.customer_id,
            company_name = customer.company_name,
            contact_name = customer.contact_name,
            contact_title = customer.contact_title,
            address = customer.address,
            city = customer.city,
            region = customer.region,
            postal_code = customer.postal_code,
            country = customer.country,
            phone = customer.phone,
            fax = customer.fax
        });

        return affectedRows > 0;
    }

    public bool Update(Customer customer)
    {
        using var connection = _connectionFactory.GetDbConnection;
        /* var query = "customers_update";
        var parameters = new DynamicParameters();
        parameters.Add("customer_id", customer.customer_id);
        parameters.Add("company_name", customer.company_name);
        parameters.Add("contact_name", customer.contact_name);
        parameters.Add("contact_title", customer.contact_title);
        parameters.Add("address", customer.address);
        parameters.Add("city", customer.city);
        parameters.Add("region", customer.region);
        parameters.Add("postal_code", customer.postal_code);
        parameters.Add("country", customer.country);
        parameters.Add("phone", customer.phone);
        parameters.Add("fax", customer.fax);
        var affectedRows = connection.Execute(query, param: parameters, commandType: CommandType.StoredProcedure); */

        var query = @"UPDATE customers
                    SET
                        company_name=@company_name,
                        contact_name=@contact_name,
                        contact_title=@contact_title,
                        address=@address,
                        city=@city,
                        region=@region,
                        postal_code=@postal_code,
                        country=@country,
                        phone=@phone,
                        fax=@fax
                    WHERE customer_id=customer_id;";
        var affectedRows = connection.Execute(query, customer);

        return affectedRows > 0;
    }

    public bool Delete(string customerId)
    {
        using var connection = _connectionFactory.GetDbConnection;
        /* var query = "customers_delete";
        var parameters = new DynamicParameters();
        parameters.Add("customer_id", customerId);
        var affectedRows = connection.Execute(query, param: parameters, commandType: CommandType.StoredProcedure); */

        var query = "DELETE FROM customers WHERE customer_id=@customer_id;";
        var affectedRows = connection.Execute(query, new { customer_id = customerId });

        return affectedRows > 0;
    }

    public Customer Get(string customerId)
    {
        using var connection = _connectionFactory.GetDbConnection;
        // var query = "customers_get_by_id";
        // var parameters = new DynamicParameters();
        // parameters.Add("customer_id", customerId);
        // var result = connection.QuerySingle(query, param: parameters, commandType: CommandType.StoredProcedure);

        var query = @"SELECT customer_id, company_name, contact_name, contact_title, address, city, region, postal_code, country, phone, fax
                        FROM customers
                        WHERE customer_id=@customer_id;";
        var result = connection.QueryFirstOrDefault<Customer>(query, new { customer_id = new DbString { Value = customerId } });

        return result;
    }

    public IEnumerable<Customer> GetAll()
    {
        using var connection = _connectionFactory.GetDbConnection;
        // var query = "customers_list";
        // var result = connection.Query<Customer>(query, commandType: CommandType.StoredProcedure);

        var query = @"SELECT customer_id, company_name, contact_name, contact_title, address, city, region, postal_code, country, phone, fax
                        FROM customers;";
        var result = connection.Query<Customer>(query);

        return result;
    }

    #endregion Sync Methods

    #region Async Methods

    public async Task<bool> InsertAsync(Customer customer)
    {
        using var connection = _connectionFactory.GetDbConnection;
        /* var query = "customers_insert";
        var parameters = new DynamicParameters();
        parameters.Add("customer_id", customer.customer_id);
        parameters.Add("company_name", customer.company_name);
        parameters.Add("contact_name", customer.contact_name);
        parameters.Add("contact_title", customer.contact_title);
        parameters.Add("address", customer.address);
        parameters.Add("city", customer.city);
        parameters.Add("region", customer.region);
        parameters.Add("postal_code", customer.postal_code);
        parameters.Add("country", customer.country);
        parameters.Add("phone", customer.phone);
        parameters.Add("fax", customer.fax);
        var affectedRows = await connection.ExecuteAsync(query, param: parameters, commandType: CommandType.StoredProcedure); */

        var query = @"INSERT INTO customers (customer_id, company_name, contact_name, contact_title, address, city, region, postal_code, country, phone, fax)
                        VALUES(@customer_id, @company_name, @contact_name, @contact_title, @address, @city, @region, @postal_code, @country, @phone, @fax);";
        var affectedRows = await connection.ExecuteAsync(query, new
        {
            customer_id = customer.customer_id,
            company_name = customer.company_name,
            contact_name = customer.contact_name,
            contact_title = customer.contact_title,
            address = customer.address,
            city = customer.city,
            region = customer.region,
            postal_code = customer.postal_code,
            country = customer.country,
            phone = customer.phone,
            fax = customer.fax
        });

        return affectedRows > 0;
    }

    public async Task<bool> UpdateAsync(Customer customer)
    {
        using var connection = _connectionFactory.GetDbConnection;
        /* var query = "customers_update";
        var parameters = new DynamicParameters();
        parameters.Add("customer_id", customer.customer_id);
        parameters.Add("company_name", customer.company_name);
        parameters.Add("contact_name", customer.contact_name);
        parameters.Add("contact_title", customer.contact_title);
        parameters.Add("address", customer.address);
        parameters.Add("city", customer.city);
        parameters.Add("region", customer.region);
        parameters.Add("postal_code", customer.postal_code);
        parameters.Add("country", customer.country);
        parameters.Add("phone", customer.phone);
        parameters.Add("fax", customer.fax);
        var affectedRows = await connection.ExecuteAsync(query, param: parameters, commandType: CommandType.StoredProcedure); */

        var query = @"UPDATE customers
                    SET
                        company_name=@company_name,
                        contact_name=@contact_name,
                        contact_title=@contact_title,
                        address=@address,
                        city=@city,
                        region=@region,
                        postal_code=@postal_code,
                        country=@country,
                        phone=@phone,
                        fax=@fax
                    WHERE customer_id=customer_id;";
        var affectedRows = await connection.ExecuteAsync(query, customer);

        return affectedRows > 0;
    }

    public async Task<bool> DeleteAsync(string customerId)
    {
        using var connection = _connectionFactory.GetDbConnection;
        /* var query = "customers_delete";
        var parameters = new DynamicParameters();
        parameters.Add("CustomerID", customerId);
        var affectedRows = await connection.ExecuteAsync(query, param: parameters, commandType: CommandType.StoredProcedure); */

        var query = "DELETE FROM customers WHERE customer_id=@customer_id;";
        var affectedRows = await connection.ExecuteAsync(query, new { customer_id = customerId });

        return affectedRows > 0;
    }

    public async Task<Customer> GetAsync(string customerId)
    {
        using var connection = _connectionFactory.GetDbConnection;
        /* var query = "customers_get_by_id";
        var parameters = new DynamicParameters();
        parameters.Add("CustomerID", customerId);
        var result = await connection.QuerySingleAsync(query, param: parameters, commandType: CommandType.StoredProcedure); */

        var query = @"SELECT customer_id, company_name, contact_name, contact_title, address, city, region, postal_code, country, phone, fax
                        FROM customers
                        WHERE customer_id=@customer_id;";
        var result = await connection.QueryFirstOrDefaultAsync<Customer>(query, new { customer_id = new DbString { Value = customerId } });

        return result;
    }

    public async Task<IEnumerable<Customer>> GetAllAsync()
    {
        using var connection = _connectionFactory.GetDbConnection;
        /* var query = "customers_list";
        var result = await connection.QueryAsync<Customer>(query, commandType: CommandType.StoredProcedure); */

        var query = @"SELECT customer_id, company_name, contact_name, contact_title, address, city, region, postal_code, country, phone, fax
                        FROM customers;";
        var result = await connection.QueryAsync<Customer>(query);

        return result;
    }

    #endregion Async Methods
}
