using Huarcaya.Ecommerce.Domain.Entity;

namespace Huarcaya.Ecommerce.Infrastructure.Interface;

public interface IUserRepository
{
    User FindOneByUsername(string username);

    User CreateNewUSer(string username, string password, string first_name, string last_name);
}