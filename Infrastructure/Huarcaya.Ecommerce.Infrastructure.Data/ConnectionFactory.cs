﻿using System.Data;

using Huarcaya.Ecommerce.Transversal.Common;

using Microsoft.Extensions.Configuration;

using Npgsql;

namespace Huarcaya.Ecommerce.Infrastructure.Data;

public class ConnectionFactory : IConnectionFactory
{
    private readonly IConfiguration _configuration;

    public ConnectionFactory(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public IDbConnection GetDbConnection
    {
        get
        {
            // var connString = "Host=localhost;Username=postgres;Password=icanux;Database=northwind";
            var connString = _configuration.GetConnectionString("NorthwindConnection");
            var sqlConnection = new NpgsqlConnection(connString);
            sqlConnection.Open();
            return sqlConnection;
        }
    }
}
