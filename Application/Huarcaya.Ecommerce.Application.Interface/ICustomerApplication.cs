﻿using Huarcaya.Ecommerce.Application.DTO;
using Huarcaya.Ecommerce.Transversal.Common;

namespace Huarcaya.Ecommerce.Application.Interface;

public interface ICustomerApplication
{
    #region Sync Methods

    Response<bool> Insert(CustomerDto customerDto);

    Response<bool> Update(CustomerDto customerDto);

    Response<bool> Delete(string customerId);

    Response<CustomerDto> Get(string customerId);

    Response<IEnumerable<CustomerDto>> GetAll();

    #endregion Sync Methods

    #region Async Methods

    Task<Response<bool>> InsertAsync(CustomerDto customerDto);

    Task<Response<bool>> UpdateAsync(CustomerDto customerDto);

    Task<Response<bool>> DeleteAsync(string customerId);

    Task<Response<CustomerDto>> GetAsync(string customerId);

    Task<Response<IEnumerable<CustomerDto>>> GetAllAsync();

    #endregion Async Methods
}
