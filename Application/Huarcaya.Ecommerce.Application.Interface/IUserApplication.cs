using Huarcaya.Ecommerce.Application.DTO;
using Huarcaya.Ecommerce.Transversal.Common;

namespace Huarcaya.Ecommerce.Application.Interface;

public interface IUserApplication
{
    Response<UserDto> Authenticate(string username, string password);

    Response<UserDto> CreateNewUSer(string username, string password, string first_name, string last_name);
}