﻿using AutoMapper;

using Huarcaya.Ecommerce.Application.DTO;
using Huarcaya.Ecommerce.Application.Interface;
using Huarcaya.Ecommerce.Domain.Entity;
using Huarcaya.Ecommerce.Domain.Interface;
using Huarcaya.Ecommerce.Transversal.Common;

namespace Huarcaya.Ecommerce.Application.Main;

public class CustomerApplication : ICustomerApplication
{
    private readonly ICustomerDomain _customerDomain;
    private readonly IMapper _mapper;
    private readonly IAppLogger<CustomerApplication> _logger;

    public CustomerApplication(ICustomerDomain customerDomain, IMapper mapper, IAppLogger<CustomerApplication> logger)
    {
        _customerDomain = customerDomain;
        _mapper = mapper;
        _logger = logger;
    }

    #region Sync Methods

    public Response<bool> Insert(CustomerDto customerDto)
    {
        var response = new Response<bool>();
        try
        {
            var customer = _mapper.Map<Customer>(customerDto);
            response.Data = _customerDomain.Insert(customer);

            if (response.Data)
            {
                response.IsSuccess = true;
                response.Message = "Registro exitoso!";
            }
        }
        catch (Exception e)
        {
            response.Message = e.Message;
        }

        return response;
    }

    public Response<bool> Update(CustomerDto customerDto)
    {
        var response = new Response<bool>();
        try
        {
            var customer = _mapper.Map<Customer>(customerDto);
            response.Data = _customerDomain.Update(customer);

            if (response.Data)
            {
                response.IsSuccess = true;
                response.Message = "Actualizacion exitosa!";
            }
        }
        catch (Exception e)
        {
            response.Message = e.Message;
        }

        return response;
    }

    public Response<bool> Delete(string customerId)
    {
        var response = new Response<bool>();
        try
        {
            response.Data = _customerDomain.Delete(customerId);

            if (response.Data)
            {
                response.IsSuccess = true;
                response.Message = "Eliminacion exitosa!";
            }
        }
        catch (Exception e)
        {
            response.Message = e.Message;
        }

        return response;
    }

    public Response<CustomerDto> Get(string customerId)
    {
        var response = new Response<CustomerDto>();
        try
        {
            var customer = _customerDomain.Get(customerId);
            response.Data = _mapper.Map<CustomerDto>(customer);

            if (response.Data != null)
            {
                response.IsSuccess = true;
                response.Message = "Consulta exitosa!";
            }
        }
        catch (Exception e)
        {
            response.Message = e.Message;
        }

        return response;
    }

    public Response<IEnumerable<CustomerDto>> GetAll()
    {
        var response = new Response<IEnumerable<CustomerDto>>();
        try
        {
            var customers = _customerDomain.GetAll();
            response.Data = _mapper.Map<IEnumerable<CustomerDto>>(customers);

            if (response.Data != null)
            {
                response.IsSuccess = true;
                response.Message = "Consulta exitosa!";
                _logger.LogInformation("Consulta exitosa.");
            }
        }
        catch (Exception e)
        {
            response.Message = e.Message;
            _logger.LogError(e.Message);
        }

        return response;
    }

    #endregion Sync Methods

    #region Async Methods

    public async Task<Response<bool>> InsertAsync(CustomerDto customerDto)
    {
        var response = new Response<bool>();
        try
        {
            var customer = _mapper.Map<Customer>(customerDto);
            response.Data = await _customerDomain.InsertAsync(customer);

            if (response.Data)
            {
                response.IsSuccess = true;
                response.Message = "Registro exitoso!";
            }
        }
        catch (Exception e)
        {
            response.Message = e.Message;
        }

        return response;
    }

    public async Task<Response<bool>> UpdateAsync(CustomerDto customerDto)
    {
        var response = new Response<bool>();
        try
        {
            var customer = _mapper.Map<Customer>(customerDto);
            response.Data = await _customerDomain.UpdateAsync(customer);

            if (response.Data)
            {
                response.IsSuccess = true;
                response.Message = "Actualizacion exitosa!";
            }
        }
        catch (Exception e)
        {
            response.Message = e.Message;
        }

        return response;
    }

    public async Task<Response<bool>> DeleteAsync(string customerId)
    {
        var response = new Response<bool>();
        try
        {
            response.Data = await _customerDomain.DeleteAsync(customerId);

            if (response.Data)
            {
                response.IsSuccess = true;
                response.Message = "Eliminacion exitosa!";
            }
        }
        catch (Exception e)
        {
            response.Message = e.Message;
        }

        return response;
    }

    public async Task<Response<CustomerDto>> GetAsync(string customerId)
    {
        var response = new Response<CustomerDto>();
        try
        {
            var customer = await _customerDomain.GetAsync(customerId);
            response.Data = _mapper.Map<CustomerDto>(customer);

            if (response.Data != null)
            {
                response.IsSuccess = true;
                response.Message = "Consulta exitosa!";
            }
        }
        catch (Exception e)
        {
            response.Message = e.Message;
        }

        return response;
    }

    public async Task<Response<IEnumerable<CustomerDto>>> GetAllAsync()
    {
        var response = new Response<IEnumerable<CustomerDto>>();
        try
        {
            var customers = await _customerDomain.GetAllAsync();
            response.Data = _mapper.Map<IEnumerable<CustomerDto>>(customers);

            if (response.Data != null)
            {
                response.IsSuccess = true;
                response.Message = "Consulta exitosa!";
            }
        }
        catch (Exception e)
        {
            response.Message = e.Message;
        }

        return response;
    }

    #endregion Async Methods
}
