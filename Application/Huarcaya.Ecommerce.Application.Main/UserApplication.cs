using AutoMapper;

using BC = BCrypt.Net.BCrypt;

using Huarcaya.Ecommerce.Application.DTO;
using Huarcaya.Ecommerce.Application.Interface;
using Huarcaya.Ecommerce.Domain.Interface;
using Huarcaya.Ecommerce.Transversal.Common;

namespace Huarcaya.Ecommerce.Application.Main;

public class UserApplication : IUserApplication
{
    private readonly IUserDomain _userDomain;
    private readonly IMapper _mapper;

    public UserApplication(IUserDomain userDomain, IMapper mapper)
    {
        _userDomain = userDomain;
        _mapper = mapper;
    }

    public Response<UserDto> Authenticate(string username, string password)
    {
        var response = new Response<UserDto>();
        if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
        {
            response.Message = "Parametros no pueden ser vacios";
            return response;
        }

        try
        {
            var user = _userDomain.FindOneByUsername(username);

            if (BC.Verify(password, user.password))
            {
                response.Data = _mapper.Map<UserDto>(user);
                response.IsSuccess = true;
                response.Message = "Autentication exitosa";
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Credenciales incorrectos";
            }
        }
        catch (InvalidOperationException)
        {
            response.IsSuccess = true;
            response.Message = "Usuario no existe";
        }
        catch (Exception e)
        {

            response.Message = e.Message;
        }

        return response;
    }

    public Response<UserDto> CreateNewUSer(string username, string password, string first_name, string last_name)
    {
        var response = new Response<UserDto>();

        try
        {
            var user = _userDomain.CreateNewUSer(username, password, first_name, last_name);
            response.Data = _mapper.Map<UserDto>(user);
            response.IsSuccess = true;
            response.Message = "Usuario registrado";
        }
        catch (InvalidOperationException)
        {
            response.IsSuccess = false;
            response.Message = "Error al registrar el usuario";
        }
        catch (Exception e)
        {

            response.Message = e.Message;
        }


        response.Data.Password = string.Empty;

        return response;
    }
}