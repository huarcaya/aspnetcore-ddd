using System.ComponentModel.DataAnnotations;

namespace Huarcaya.Ecommerce.Application.DTO;

public record UserDto
{
    public int UserId { get; set; }
    [Required]
    public string Username { get; set; }
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Token { get; set; }
}