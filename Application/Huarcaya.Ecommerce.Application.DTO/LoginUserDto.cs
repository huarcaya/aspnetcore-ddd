using System.ComponentModel.DataAnnotations;

namespace Huarcaya.Ecommerce.Application.DTO;

public record LoginUserDto

{
    [Required]
    public string Username { get; set; }
    [Required]
    public string Password { get; set; }
}