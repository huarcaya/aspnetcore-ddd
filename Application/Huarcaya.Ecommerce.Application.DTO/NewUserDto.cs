using System.ComponentModel.DataAnnotations;

namespace Huarcaya.Ecommerce.Application.DTO;

public record NewUserDto

{
    [Required]
    public string Username { get; set; }
    [Required]
    public string Password { get; set; }
    [Required]
    public string FirstName { get; set; }
    [Required]
    public string LastName { get; set; }
}