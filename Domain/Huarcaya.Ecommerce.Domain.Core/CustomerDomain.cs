﻿using Huarcaya.Ecommerce.Domain.Entity;
using Huarcaya.Ecommerce.Domain.Interface;
using Huarcaya.Ecommerce.Infrastructure.Interface;

namespace Huarcaya.Ecommerce.Domain.Core;

public class CustomerDomain : ICustomerDomain
{
    private readonly ICustomerRepository _customerRepository;

    public CustomerDomain(ICustomerRepository customerRepository)
    {
        _customerRepository = customerRepository;
    }

    #region Sync Methods

    public bool Insert(Customer customer)
    {
        return _customerRepository.Insert(customer);
    }

    public bool Update(Customer customer)
    {
        return _customerRepository.Update(customer);
    }

    public bool Delete(string customerId)
    {
        return _customerRepository.Delete(customerId);
    }

    public Customer Get(string customerId)
    {
        return _customerRepository.Get(customerId);
    }

    public IEnumerable<Customer> GetAll()
    {
        return _customerRepository.GetAll();
    }

    #endregion Sync Methods

    #region Async Methods

    public async Task<bool> InsertAsync(Customer customer)
    {
        return await _customerRepository.InsertAsync(customer);
    }

    public async Task<bool> UpdateAsync(Customer customer)
    {
        return await _customerRepository.UpdateAsync(customer);
    }

    public async Task<bool> DeleteAsync(string customerId)
    {
        return await _customerRepository.DeleteAsync(customerId);
    }

    public async Task<Customer> GetAsync(string customerId)
    {
        return await _customerRepository.GetAsync(customerId);
    }

    public async Task<IEnumerable<Customer>> GetAllAsync()
    {
        return await _customerRepository.GetAllAsync();
    }

    #endregion Async Methods
}
