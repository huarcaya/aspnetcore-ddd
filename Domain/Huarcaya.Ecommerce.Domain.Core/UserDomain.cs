using Huarcaya.Ecommerce.Domain.Entity;
using Huarcaya.Ecommerce.Domain.Interface;
using Huarcaya.Ecommerce.Infrastructure.Interface;

namespace Huarcaya.Ecommerce.Domain.Core;

public class UserDomain : IUserDomain
{
    private readonly IUserRepository _userRepository;

    public UserDomain(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public User FindOneByUsername(string username)
    {
        return _userRepository.FindOneByUsername(username);
    }

    public User CreateNewUSer(string username, string password, string first_name, string last_name)
    {
        return _userRepository.CreateNewUSer(username, password, first_name, last_name);
    }
}