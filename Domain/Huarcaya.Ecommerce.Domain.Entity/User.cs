namespace Huarcaya.Ecommerce.Domain.Entity;

public class User
{
    public int user_id { get; set; }
    public string username { get; set; }
    public string password { get; set; }
    public string first_name { get; set; }
    public string last_name { get; set; }
    public string token { get; set; }
}