﻿using Huarcaya.Ecommerce.Domain.Entity;

namespace Huarcaya.Ecommerce.Domain.Interface;

public interface ICustomerDomain
{
    #region Sync Methods

    bool Insert(Customer customer);

    bool Update(Customer customer);

    bool Delete(string customerId);

    Customer Get(string customerId);

    IEnumerable<Customer> GetAll();

    #endregion Sync Methods

    #region Async Methods

    Task<bool> InsertAsync(Customer customer);

    Task<bool> UpdateAsync(Customer customer);

    Task<bool> DeleteAsync(string customerId);

    Task<Customer> GetAsync(string customerId);

    Task<IEnumerable<Customer>> GetAllAsync();

    #endregion Async Methods
}
