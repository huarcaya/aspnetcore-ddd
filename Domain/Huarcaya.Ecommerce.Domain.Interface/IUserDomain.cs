using Huarcaya.Ecommerce.Domain.Entity;

namespace Huarcaya.Ecommerce.Domain.Interface;

public interface IUserDomain
{
    User FindOneByUsername(string username);

    User CreateNewUSer(string username, string password, string first_name, string last_name);
}